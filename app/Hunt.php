<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Hunt extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voshunts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['time'];

    public function getLongLat()
    {

        if (Cache::has('geocode-' . $this->id)) {
            return Cache::get('geocode-' . $this->id);
        } else {
            //Get Response form google:
//            dd('https://nominatim.openstreetmap.org/search?q=' . urlencode($this->location) . '&format=json&polygon=1&addressdetails=1');
//            $payload = file_get_contents('https://nominatim.openstreetmap.org/search?q=' . urlencode($this->location) . '&format=json&polygon=1&addressdetails=0');
//
//            $payload = json_decode($payload, true);
            $payload = ($this->geocode($this->location));

            if ($payload) {

                $return = $payload[1] . ', ' . $payload[0];

                Cache::forever('geocode-' . $this->id, $return);

            } else {
                $return = '0, 0';
            }
            return $return;
        }

    }


    function geocode($address)
    {
        // url encode the address
        $address = urlencode($address);

        $email = urlencode('development@cbyte.nl');

        $url = "http://nominatim.openstreetmap.org/?format=json&addressdetails=1&q={$address}&format=json&limit=1&email={$email}";

        // get the json response
        $resp_json = file_get_contents($url);

        // decode the json
        $resp = json_decode($resp_json, true);

        if(isset($resp[0]['lat']))
            return array($resp[0]['lat'], $resp[0]['lon']);

        return false;
    }
}
