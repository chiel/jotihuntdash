<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'assignment_endtime'
    ];

//    public function pharsedPostBody() {
//        $content = $this->post_content;
//
//        $content = html_entity_decode($content);
//        $content = str_replace('src="/', 'src="http://jotihunt.net/', $content);
//
//        $content = str_replace('<a ', '<a target="_blank"', $content);
//
//        return $content;
//    }

}
