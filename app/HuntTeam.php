<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HuntTeam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'huntteams';

    public function printTeamMembers() {
        $teamMembers = $this->team_members;

        $teamMembers = json_decode($teamMembers);

        foreach ($teamMembers as $key => $values) {
            $teamMembers[$key] = '<i class="fa fa-user" aria-hidden="true"></i> ' . $teamMembers[$key];
        }

        return implode(" , ", $teamMembers);
    }

    public function getTeamMembersCSV() {
        return implode(',', json_decode($this->team_members));
    }

    public function getLongLat() {
        return '' . $this->long . ', ' . $this->lat . '';
    }
}
