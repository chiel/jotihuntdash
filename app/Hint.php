<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hint extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voslocations';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['hint_time'];


    public function hasMarker() {
        return ($this->lat > 0 && $this->long > 0);
    }

    public function getLongLat() {
       return '' . $this->long . ', ' . $this->lat . '';
    }
}
