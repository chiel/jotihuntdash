<?php

namespace App\Http\Controllers;

use App\Hint;
use App\Http\Requests\AddHintRequest;
use App\Http\Requests\AddHuntRequest;
use App\Hunt;
use App\HuntTeam;
use App\VosStatus;
use Carbon\Carbon;

class VosController extends Controller
{
    private $teamNames = ['Alpha', 'Bravo', 'Charlie', 'Delta', 'Echo', 'Foxtrot'];

    public function view($teamName) {
        $vos = VosStatus::where('team', $teamName)->orderBy('created_at', 'DESC')->firstOrFail();

        $teams = HuntTeam::where('hunts_on_team', $teamName)->get();

        $hunts = Hunt::where('team', $teamName)->orderBy('time', 'DESC')->get();

        $hints = Hint::where('team', $teamName)->orderBy('hint_time', 'DESC')->get();

        return view('vossen.vos')
                    ->with('vos', $vos)
                    ->with('teams', $teams)
                    ->with('hunts', $hunts)
                    ->with('hints', $hints)
                    ->with('autorefresh', true);

    }

    public function addHunt($teamName, AddHuntRequest $huntRequest) {
        //Protect agains non-existing teams
        $this->guardValidTeamName($teamName);

        $newHunt        = new Hunt();
        $newHunt->team  = $teamName;

        $newHunt->location  = $huntRequest->location;
        $newHunt->code      = $huntRequest->code;
        $newHunt->time      = Carbon::parse($huntRequest->time);

        $newHunt->save();

        return redirect()->back();
    }

    public function deleteHunt($teamName, $huntId) {
        $hunt = Hunt::findOrFail($huntId);

        $hunt->delete();

        return redirect()->back();
    }

    public function addHint($teamName, AddHintRequest $hintRequest) {
        //Protect agains non-existing teams
        $this->guardValidTeamName($teamName);

        $hint       = new Hint();
        $hint->team = $teamName;

        $hint->location_human   = $hintRequest->location;
        $hint->hint_time        = strtotime($hintRequest->hint_time);
        $hint->long = ($hintRequest->long > 0) ? $hintRequest->long : 0;
        $hint->lat = ($hintRequest->lat > 0) ? $hintRequest->lat  : 0;

        //dd($hint);

        $hint->save();

        return redirect()->back();

    }

    public function deleteHint($teamName, $hintId) {
        $hunt = Hint::findOrFail($hintId);

        $hunt->delete();

        return redirect()->back();
    }

    public function getKml($teamName = null) {
        if($teamName == null) {
            $hunts = Hunt::all();
        } else {
            $hunts = Hunt::where('team', $teamName)->get();
        }

        return view('voskml', ['hunts' => $hunts]);
    }




    /*
     * Checks if teamName is a known teamName else throw a 404.
     */
    private function guardValidTeamName($teamName) {
        if(in_array($teamName, $this->teamNames)) {
            return true;
        } else {
            abort(404, 'Team niet gevonden');
        }
    }
}
