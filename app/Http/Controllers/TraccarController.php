<?php

namespace App\Http\Controllers;

use App\HuntTeam;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class TraccarController extends Controller
{
    private function sendRequest($method, $uri, $requestOptions = null)
    {
        $client = new Client();

        $guzzleRequestOptions = [
                'auth' => [env('TRACCAR_EMAIL'), env('TRACCAR_PASSWORD')]
        ];
        if(!empty($requestOptions)){
            $guzzleRequestOptions = array_merge($guzzleRequestOptions, $requestOptions);
        }
        try {
            $res = $client->request($method, env('TRACCAR_URL') . $uri, $guzzleRequestOptions);
        } catch (GuzzleException $e) {
            return null;
        }
        // params to debug with
        // $res->getStatusCode();
        // "200"
        // $res->getHeader('content-type')
        // 'application/json; charset=utf8'

        if($res->getStatusCode() === 200 && $res->getHeader('Content-Type') === ['application/json']) {
            return json_decode($res->getBody());
        }
        return null;
    }

    public function getLastDeviceLocation($deviceUniqueId)
    {
        // get the last position of this device
        $device = $this->sendRequest('GET', 'devices?uniqueId=' . $deviceUniqueId);
        if (empty($device)){
            return;
        }
        $positionId = $device[0]->positionId;

        $position = $this->sendRequest('GET', 'positions?id=' . $positionId);
        if (empty($position)){
            return;
        }
        return [
            'long' => $position[0]->longitude,
            'lat' => $position[0]->latitude,
            'batterylevel' => $position[0]->attributes->batteryLevel,
        ];
    }

    public function updateLastDevicesLocation(){
        $huntteams = HuntTeam::all();
        foreach ($huntteams as $huntteam){
            if (!empty($huntteam->device_uniqueId)){
                $response = $this->getLastDeviceLocation($huntteam->device_uniqueId);
                if(!empty($response)){
                    if(!empty($response['lat'])){
                        $huntteam->lat = $response['lat'];
                    }
                    if(!empty($response['long'])){
                        $huntteam->long = $response['long'];
                    }
                    // intentionaly not checked. if u switch device and that one does not support batterylevel
                    // you dont want to get the battery level of the previous device
                    $huntteam->batterylevel = $response['batterylevel'];
                    $huntteam->save();
                }
            }
        }
    }

    public function createNewDevice($deviceUniqueId, $name){
        $requestOptions = ['json' =>
            ['uniqueId' => $deviceUniqueId,
            'name' => $name]
        ];
        // if we want to add a device which already exists, we will get an error on the sendRequest method
        // this error is irrelevant for us because
        // if this device is already there we will then only update the records in our database
        // and we do not mess something up in the traccar database
        $this->sendRequest("POST", 'devices', $requestOptions);
    }

}
