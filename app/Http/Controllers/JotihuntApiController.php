<?php

namespace App\Http\Controllers;

use App\Hint;
use App\Hunt;
use App\HuntTeam;
use App\Post;
use App\VosStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Response;

class JotihuntApiController extends Controller
{
    private $endPoint = "https://jotihunt.net/api/";
    private $apiVersion = "1.0";

    public function fetch() {
        $this->fetchNews();
        $this->fetchAssingments();
        $this->fetchHints();

        $this->fetchVossen();
    }


    public function fetchVossen() {
        $vossen = $this->getPathContents('vossen');

        foreach($vossen as $vos) {
            $status = VosStatus::firstOrNew(['team' => $vos['team']]);
            $status->team = $vos['team'];
            $status->status = $vos['status'];

            $status->save();
        }
    }



    /*
     * Fetches full /news action of the API
     */
    public function fetchNews() {
        $news = $this->getPathContents('nieuws');

        foreach ($news as $newsItem) {
            $newsItemDetail = $this->getPathContents('nieuws/' . $newsItem['ID']);
            if(!Post::where('post_id', '=', $newsItem['ID'])->exists()) {
                $post = new Post();
                $post->post_type = 'nieuws';
                $post->post_id = $newsItem['ID'];
                $post->post_title = $newsItem['titel'];
                $post->post_content = $newsItemDetail[0]['inhoud'];
                $post->post_date = date('Y-m-d H:i:s', strtotime($newsItemDetail[0]['datum']));
                $post->post_update = date('Y-m-d H:i:s', strtotime($newsItem['lastupdate']));
                $post->save();
            }

//            DB::insert('INSERT IGNORE INTO posts SET
//                          post_type = "nieuws",
//                          post_id = ' . $newsItem['ID'] . ',
//                          post_title = "' . $newsItem['titel'] . '",
//                          post_content = "' . $newsItemDetail[0]['inhoud'] . '",
//                          post_date = "' . date('Y-m-d H:i:s', strtotime($newsItemDetail[0]['datum'])) .'",
//                          post_update = "' . date('Y-m-d H:i:s', strtotime($newsItem['lastupdate'])) .'",
//                          updated_at = "' . date('Y-m-d H:i:s', time()) .'",
//                          created_at = "' . date('Y-m-d H:i:s', time()) .'"');
        }
    }

    /*
     * Fetches all assignments
     */
    public function fetchAssingments() {
        $assingments = $this->getPathContents('opdracht');

        foreach ($assingments as $newsItem) {
            $newsItemDetail = $this->getPathContents('opdracht/' . $newsItem['ID']);
            if(!Post::where('post_id', '=', $newsItem['ID'])->exists()) {
                $post = new Post();
                $post->post_type = 'opdracht';
                $post->post_id = $newsItem['ID'];
                $post->post_title = $newsItem['titel'];
                $post->post_content = $newsItemDetail[0]['inhoud'];
                $post->post_date = date('Y-m-d H:i:s', strtotime($newsItemDetail[0]['datum']));
                $post->post_update = date('Y-m-d H:i:s', strtotime($newsItem['lastupdate']));
                $post->assignment_endtime = date('Y-m-d H:i:s', strtotime($newsItem['eindtijd']));
                $post->assignment_points = $newsItem['maxpunten'];
                $post->save();
            }
        }
    }

    /*
 * Fetches full /news action of the API
 */
    public function fetchHints() {
        $hits = $this->getPathContents('hint');

        foreach ($hits as $hintItem) {
            //dd($hintItem);
            $hintItemDetail = $this->getPathContents('hint/' . $hintItem['ID']);


            $body = $hintItemDetail[0]['inhoud'] . '<p>';

            foreach(TeamController::$teamNames as $teamName) {
                if(key_exists($teamName, $hintItemDetail[0])) {
                    foreach ($hintItemDetail[0][$teamName] as $img) {
                        $body .= '<img src="' . $img . '" class="img-responsive" style="max-width: 20%" />';
                    }
                }
            }

            $body .= '</p>';
            if(!Post::where('post_id', '=', $hintItem['ID'])->exists()) {
                $post = new Post();
                $post->post_type = 'hint';
                $post->post_id = $hintItem['ID'];
                $post->post_title = $hintItem['titel'];
                $post->post_content = $body;
                $post->post_date = date('Y-m-d H:i:s', strtotime($hintItemDetail[0]['datum']));
                $post->post_update = date('Y-m-d H:i:s', strtotime($hintItem['lastupdate']));
                $post->save();
            }
        }
    }


    /*
     * Executes the API command
     */
    private function getPathContents($action) {
        $completePath = $this->endPoint . $this->apiVersion . '/' . $action;

        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $completePath);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        if(key_exists('data', json_decode($output, true))) {
            return json_decode($output, true)['data'];
        } else {
            return [];
        }
    }



    /*
     * Should refresh checks most recent entry. If is new entry, update page.
     */
    public function shouldRefresh() {
        $response = [
            'lastTimestamp' => self::getLastTimestamp()
        ];

        return Response::json($response);
    }

    public static function getLastTimestamp() {
        $lastTimestamps = collect();

        if(Post::orderBy('created_at', 'DESC')->first()) {
            $lastTimestamps->push(Post::orderBy('updated_at', 'DESC')->first()->updated_at->timestamp);
        }

        if(Hint::orderBy('created_at', 'DESC')->first()) {
            $lastTimestamps->push(Hint::orderBy('updated_at', 'DESC')->first()->updated_at->timestamp);
        }

        if(Hunt::orderBy('created_at', 'DESC')->first()) {
            $lastTimestamps->push(Hunt::orderBy('updated_at', 'DESC')->first()->updated_at->timestamp);
        }

        if(VosStatus::orderBy('created_at', 'DESC')->first()) {
            $lastTimestamps->push(VosStatus::orderBy('updated_at', 'DESC')->first()->updated_at->timestamp);
        }

        if(HuntTeam::orderBy('created_at', 'DESC')->first()) {
            $lastTimestamps->push(HuntTeam::orderBy('updated_at', 'DESC')->first()->updated_at->timestamp);
        }

        return $lastTimestamps->max();
    }
}
