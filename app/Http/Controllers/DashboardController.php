<?php

namespace App\Http\Controllers;

use App\Hint;
use App\Hunt;
use App\HuntTeam;
use App\Post;
use App\VosStatus;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        $vossenData = VosStatus::orderBy('team', 'ASC')->limit(6)->get();

        foreach ($vossenData as $key => $vos) {
            //Get Last hint
            $hint = Hint::where('team', $vos->team)->orderBy('hint_time', 'DESC')->first();

            //Get Last Hunt
            $hunt = Hunt::where('team', $vos->team)->orderBy('time', 'DESC')->first();

            $newElement  = [
                'vos' => $vos,
                'hint' => $hint,
                'hunt' => $hunt
            ];

            $vossenData[$key] = $newElement;
        }

        $hunters = HuntTeam::all();

        $nieuws = Post::where('post_type', 'nieuws')->orderBy('post_date', 'DESC')->limit(10)->get();
        $hints = Post::where('post_type', 'hint')->orderBy('post_date', 'DESC')->limit(10)->get();
        $opdrachten = Post::where('post_type', 'opdracht')->orderBy('post_date', 'DESC')->limit(10)->get();

        return view('dashboard')
                    ->with('vossen', $vossenData)
                    ->with('nieuws', $nieuws)
                    ->with('hints', $hints)
                    ->with('opdrachten', $opdrachten)
                    ->with('hunters', $hunters)
                    ->with('autorefresh', true);
    }

    public function index2() {
        $vossenData = VosStatus::orderBy('team', 'ASC')->limit(6)->get();

        foreach ($vossenData as $key => $vos) {
            //Get Last hint
            $hint = Hint::where('team', $vos->team)->orderBy('hint_time', 'DESC')->first();

            //Get Last Hunt
            $hunt = Hunt::where('team', $vos->team)->orderBy('time', 'DESC')->first();

            $newElement  = [
                'vos' => $vos,
                'hint' => $hint,
                'hunt' => $hunt
            ];

            $vossenData[$key] = $newElement;
        }

        $hunters = HuntTeam::all();

        $hunts = Hunt::all();
        $hints = Hint::all();
        $teams = HuntTeam::all();

        return view('dashboard2')
            ->with('vossen', $vossenData)
            ->with('hunters', $hunters)
            ->with('autorefresh', true)
            ->with('hunts', $hunts)
            ->with('hints', $hints)
            ->with('teams', $teams);
    }

    public function map() {
        $hunts = Hunt::all();
        $hints = Hint::all();
        $teams = HuntTeam::all();
        return view('map')->with('hunts', $hunts)->with('hints', $hints)->with('teams', $teams);
    }
}