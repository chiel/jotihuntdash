<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTeam;
use App\HuntTeam;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public static $teamNames = ['Alpha', 'Bravo', 'Charlie', 'Delta', 'Echo', 'Foxtrot'];

    public function index() {

        $teams = HuntTeam::all();

        return view('teams.index')->with('teams', $teams)->with('teamNames', self::$teamNames);
    }

    public function createTeam(CreateTeam $request) {

        $team = new HuntTeam();
        $team->name = $request->name;
        $team->team_members = json_encode(explode(',', $request->members));
        $team->phone = $request->phone . '';
        $team->hunts_on_team = $request->hunts_on;
        $team->device_uniqueId = $request->device_uniqueId;

        $team->save();
        $traccarApiController = new TraccarController();
        $traccarApiController->createNewDevice($team->device_uniqueId, $team->name);
        return redirect(action('TeamController@index'));
    }

    public function editTeam($id, CreateTeam $request) {

        $team = HuntTeam::findOrFail($id);
        $team->name = $request->name;
        $team->team_members = json_encode(explode(',', $request->members));
        $team->phone = $request->phone . '';
        $team->hunts_on_team = $request->hunts_on;
        $team->device_uniqueId = $request->device_uniqueId;

        $team->save();
        $traccarApiController = new TraccarController();
        $traccarApiController->createNewDevice($team->device_uniqueId, $team->name);
        return redirect(action('TeamController@index'));
    }

    public function delete($id) {
        $team = HuntTeam::findOrFail($id);
        $team->delete();

        return redirect(action('TeamController@index'));
    }
}
