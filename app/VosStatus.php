<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VosStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vosstatus';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'assignment_endtime'
    ];

    protected $fillable = ['team', 'status'];
}
