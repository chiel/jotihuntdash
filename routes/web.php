<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function () {
    return redirect('/');
});

Auth::routes();

Route::group([ 'middleware' => ['auth','valid']], function()
{
    //Map APIS
    Route::get('/kml/{vos?}', 'VosController@getKML');

    Route::get('/', 'DashboardController@index');
    Route::get('/dash2', 'DashboardController@index2');

    Route::get('/map', 'DashboardController@map');

    //Teams
    Route::get('/teams', 'TeamController@index');
    Route::post('/teams/create', 'TeamController@createTeam');
    Route::post('/teams/{id}/update', 'TeamController@editTeam');
    Route::get('/teams/{id}/delete', 'TeamController@delete');

    //Vossen
    Route::get('/vos/{vosName}', 'VosController@view');

    Route::post('/vos/{vosName}/addhunt', 'VosController@addHunt');
    Route::get('/vos/{vosName}/hunt/{id}/delete', 'VosController@deleteHunt');

    Route::post('/vos/{vosName}/addhint', 'VosController@addHint');
    Route::get('/vos/{vosName}/hint/{id}/delete', 'VosController@deleteHint');


});

Route::get('/shouldrefresh', 'JotihuntApiController@shouldRefresh');
