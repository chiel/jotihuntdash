<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('fetch-api', function () {
    $jotihuntApiController = new \App\Http\Controllers\JotihuntApiController;
    $jotihuntApiController->fetch();
});
Artisan::command('fetch-traccar-api', function(){
    $traccarApiController = new \App\Http\Controllers\TraccarController;
    $traccarApiController->updateLastDevicesLocation();
});
