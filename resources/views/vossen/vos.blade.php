@extends('base')

@section('head')
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section('page')
    <div class="row hidden-lg-up">
        <br>
        <br>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="widget stats-widget" style="margin: 0px;">
                @if($vos->status == 'rood')
                    <footer class="widget-footer bg-danger"></footer>
                @elseif($vos->status == 'oranje')
                    <footer class="widget-footer bg-warning"></footer>
                @elseif($vos->status == 'groen')
                    <footer class="widget-footer bg-success"></footer>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Begin Map -->
        <div class="col-lg-12" >
            <div id="demoMap" style="height: 300px; width: 100%"></div>
        </div>
        <br>
    </div>

    <!-- Begin Deelgebieden -->
    <!-- /.row -->
    <div class="row">
        {{--<div class="col-lg-4 col-sm-12">--}}
            {{--<div class="widget stats-widget">--}}
                {{--<div class="widget-body">--}}
                    {{--<div>--}}
                        {{--<h3 class="widget-title text-primary">{{$vos->team}}--}}
                            {{--@if(isset($hunts[0]))--}}
                                {{--<small style="color: red; font-size: 11px;">--}}
                                    {{--nieuwe hunt {{ date('H:i', strtotime('+1 hour', strtotime($hunts[0]['time']))) }}--}}
                                {{--</small>--}}
                            {{--@endif--}}
                        {{--</h3>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--@if($vos->team == 'Alpha')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Alpha-2.png" alt="Foto van vos...">--}}
                        {{--@elseif($vos->team == 'Bravo')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Bravo-2.png" alt="Foto van vos...">--}}
                        {{--@elseif($vos->team == 'Charlie')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Charlie-2.png" alt="Foto van vos...">--}}
                        {{--@elseif($vos->team == 'Delta')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Delta-2.png" alt="Foto van vos...">--}}
                        {{--@elseif($vos->team == 'Echo')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Echo-2.png" alt="Foto van vos...">--}}
                        {{--@elseif($vos->team == 'Foxtrot')--}}
                            {{--<img style="max-height: 400px" src="/vossen/Foxtrot-2.png" alt="Foto van vos...">--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- Eind Deelgebieden -->

        <!-- Eind hint -->
        <div class="col-md-4">
            <div class="widget p-4">
                <h4 class="mb-4">Teams
                    <span class="pull-right">
                        <a href="{{ action('TeamController@index') }}"><i class="fa fa-chevron-right fa-fw"></i></a>
                    </span>
                </h4>
                <table class="table table-striped">
                    <tr>
                        <th>Team</th>
                        <th>Inzittende</th>
                    </tr>
                    @foreach($teams as $team)
                        <tr>
                            <td>{{ $team->name }}</td>
                            <td>{!! $team->printTeamMembers() !!}
                                @if($team->phone != '')
                                    <a class="btn btn-primary btn-sm pull-right" href="tel:{{$team->phone}}"><i class="fa fa-phone" style="color: white;"></i></a>
                                @endif</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .widget -->
        </div>
        <!-- Eind hint -->
        <div class="col-md-4">
            <div class="widget p-4">
                <h4 class="mb-4">Hints
                    @if(\Illuminate\Support\Facades\Auth::user()->mayEdit())
                        <span class="pull-right">
                            <a href="#" data-toggle="modal" data-target="#addHint"><i class="fa fa-plus fa-fw"></i></a>
                        </span>
                    @endif
                </h4>
                <table class="table table-striped">
                    <tr>
                        <th>Locatie</th>
                        <th>Hint tijd</th>
                    </tr>

                    @foreach($hints as $hint)
                        <tr>
                            <td>{{ $hint->location_human }}</td>
                            <td>{{ $hint->hint_time->format('H:i') }}
                                @if(\Illuminate\Support\Facades\Auth::user()->mayEdit())
                                    <a class="btn btn-danger btn-sm pull-right" href="{{ action('VosController@deleteHint', [$vos->team, $hint->id]) }}"><i class="fa fa-remove" style="color: white;"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .widget -->
        </div>
        <div class="col-md-4">
            <div class="widget p-4">
                <h4 class="mb-4">Hunts
                    @if(\Illuminate\Support\Facades\Auth::user()->mayEdit())
                        <span class="pull-right">
                            <a href="#" data-toggle="modal" data-target="#addHunt"><i class="fa fa-plus fa-fw"></i></a>
                        </span>
                    @endif
                </h4>
                <table class="table table-striped">
                    <tr>
                        <th>Locatie</th>
                        <th>Code</th>
                        <th>Tijd</th>
                    </tr>
                    @foreach($hunts as $hunt)
                        <tr>
                            <td>{{ $hunt->location }}</td>
                            <td>{{ $hunt->code }}</td>
                            <td>{{ $hunt->time->format('H:i') }}
                                @if(\Illuminate\Support\Facades\Auth::user()->mayEdit())
                                    <a class="btn btn-danger btn-sm pull-right" href="{{ action('VosController@deleteHunt', [$vos->team, $hunt->id]) }}"><i class="fa fa-remove" style="color: white;"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <!-- .widget -->
        </div>
    </div>

    <div class="row">
        @include('vossen.addhunt')
        @include('vossen.addhint')
    </div>
@endsection

@section('footer')
    @include('components.maps.baseMap')
    @include('components.maps.addHunts', $hunts)
    @include('components.maps.addTeams', $teams)
    @include('components.maps.addHints', $hints)
    {{--<script async defer--}}
            {{--src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7VGliPE5Sc0EHgAj-NHk4iBuljGfUQSE&callback=initMap">--}}
    {{--</script>--}}
    {{--<script>--}}


        {{--function initMap() {--}}
            {{--@if($vos->team == 'Alpha')--}}
                {{--var myLatLng = {lat: 52.2090551, lng: 5.6866911};--}}
            {{--@elseif($vos->team == 'Bravo')--}}
                {{--var myLatLng = {lat: 52.1939645, lng: 6.0953727};--}}
            {{--@elseif($vos->team == 'Charlie')--}}
                {{--var myLatLng = {lat: 51.9517722, lng: 6.2165301};--}}
            {{--@elseif($vos->team == 'Delta')--}}
                {{--var myLatLng = {lat: 51.8798632, lng: 5.8965888};--}}
            {{--@elseif($vos->team == 'Echo')--}}
                {{--var myLatLng = {lat: 51.8528334, lng: 5.5326866};--}}
            {{--@elseif($vos->team == 'Foxtrot')--}}
                {{--var myLatLng = {lat: 51.9730295, lng: 5.5813509};--}}
            {{--@else--}}
                {{--var myLatLng = {lat: 51.882091, lng: 5.615899};--}}
            {{--@endif--}}


        {{--}--}}
    {{--</script>--}}
@endsection