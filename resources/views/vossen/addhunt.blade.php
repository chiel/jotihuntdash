<!-- Modal -->
<div id="addHunt" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Nieuw hunt toevoegen</h4>
                <small>Letop, word <b>niet</b> automatisch ingestuurd!</small>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form method="POST" action="{{ action('VosController@addHunt', $vos->team) }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="location">Locatie</label>
                        <input
                                name="location"
                                class="form-control"
                                id="location"
                                placeholder="Hoofdstraat, Ergens"
                                type="text">
                    </div>

                    <div class="form-group">
                        <label for="code">Code</label>
                        <input
                                name="code"
                                class="form-control"
                                id="code"
                                placeholder="B2VH6PV"
                                type="text">
                    </div>

                    <div class="form-group">
                        <label for="time">Moment</label>
                        <input
                                name="time"
                                class="form-control"
                                id="time"
                                type="text"
                                value="{{ \Carbon\Carbon::now()->format('Y-m-d H:i') }}">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success pull-right" type="submit">Invoeren</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>