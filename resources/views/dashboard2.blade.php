@extends('base')

@section('page')
    @include('components.vossen')
    <div class="row">
        <!-- Begin Map -->
        <div class="col-lg-12" >
            <div id="demoMap" style="height: 650px; width: 100%"></div>
        </div>
        <br>
    </div>
@endsection

@section('footer')
    @include('components.maps.baseMap')
    @include('components.maps.addHunts', $hunts)
    @include('components.maps.addTeams', $teams)
    @include('components.maps.addHints', $hints)
@endsection