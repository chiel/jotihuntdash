@extends('base')

@section('page')

        {{--<!-- Begin Map -->--}}
        {{--<div class="col-lg-12" >--}}
            {{--<div id="map" style="height: 800px; width: 100%"></div>--}}
        {{--</div>--}}
        {{--<!-- Eind Map -->--}}

        <div id="demoMap" style="height: 700px; width: 100%"></div>
@endsection


@section('footer')
    @include('components.maps.baseMap')
    @include('components.maps.addHunts', $hunts)
    @include('components.maps.addTeams', $teams)
    @include('components.maps.addHints', $hints)
        <script>


        // var kmlLayer2 = new OpenLayers.Layer.Vector("KML", {
        //     strategies: [new OpenLayers.Strategy.Fixed()],
        //     protocol: new OpenLayers.Protocol.HTTP({
        //         url: "test.kml",
        //         format: new OpenLayers.Format.KML({
        //             extractStyles: true,
        //             extractAttributes: true,
        //             maxDepth: 1
        //         })
        //     })
        // });
        // map.addLayer(kmlLayer2);


    </script>

@endsection