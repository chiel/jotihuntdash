@extends('base')

@section('page')
    <!-- Vossen Status -->
    <div class="row">
        <div class="col-lg-12">
            <div class="widget stats-widget">
                <div class="widget-body">
                    <h3 class="widget-title text-primary">Teams</h3>

                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-striped" style="width: 100%;">
                                <thead>
                                <th width="20%">Team naam</th>
                                <th>Team leden</th>
                                <th width="20%">Hunt op</th>
                                <th>Traccar device id</th>
                                <th>Traccar device battery</th>
                                <th style="width: 120px;">Acties</th>
                                </thead>
                                <tbody>
                                @foreach($teams as $team)
                                    <tr>
                                        <td>{{ $team->name }}</td>
                                        <td>{!! $team->printTeamMembers() !!}</td>
                                        <td>{{ $team->hunts_on_team }}</td>
                                        <td>{{ $team->device_uniqueId }}</td>
                                        <td>{{ $team->batterylevel }}%</td>
                                        <td>
                                            @if(\Illuminate\Support\Facades\Auth::user()->mayEdit())
                                                <a class="btn btn-danger btn-sm" href="{{ action('TeamController@delete', $team->id) }}"><i class="fa fa-remove" style="color: white;"></i></a>
                                                <a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editTeam-{{ $team->id }}"><i class="fa fa-pencil" style="color: white;"></i></a>
                                                @if($team->phone != '')
                                                    <a class="btn btn-primary btn-sm" href="tel:{{$team->phone}}"><i class="fa fa-phone" style="color: white;"></i></a>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-lg-12 ">
                            <button class="btn btn-success pull-right" href="#" data-toggle="modal" data-target="#addTeam">
                                <i class="fa fa-plus"></i> Nieuw
                            </button>
                        </div>
                    </div>

                </div>
                <footer class="widget-footer bg-primary"></footer>
            </div>
        </div>
    </div>
    <!-- / Vossen Satus -->


    <!-- Modal -->
    <div id="addTeam" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nieuw team toevoegen</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ action('TeamController@createTeam') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Team naam</label>
                            <input
                                    name="name"
                                    class="form-control"
                                    id="name"
                                    placeholder="Voorbeeld team..."
                                    type="text">
                        </div>

                        <div class="form-group">
                            <label for="members">Groeps leden</label>
                            <input
                                    name="members"
                                    class="form-control"
                                    id="members"
                                    placeholder="Jan, Klaas, Bart"
                                    type="text">
                            <small>Gescheiden door een ", "</small>
                        </div>

                        <div class="form-group">
                            <label for="phone">Telefoonnummer</label>
                            <input
                                    name="phone"
                                    class="form-control"
                                    id="phone"
                                    placeholder="0614145994"
                                    type="text">
                            <small>Zonder spaties of andere tekens, alleen nummmer aan elkaar geschreven</small>
                        </div>

                        <div class="form-group">
                            <label for="phone">Traccar device id</label>
                            <input
                                    name="device_uniqueId"
                                    class="form-control"
                                    id="device_uniqueId"
                                    placeholder="123456"
                                    type="text">
                            <small>Zonder spaties of andere tekens, alleen nummmer aan elkaar geschreven</small>
                        </div>

                        <div class="form-group">
                            <label for="hunts_on">Hunt op</label>
                            <select name="hunts_on" class="form-control" id="hunts_on">
                                @foreach($teamNames as $teamName)
                                    <option value="{{ $teamName }}"> {{$teamName}}</option>
                                @endforeach
                                <option value="Niemand (inactief)"> Niemand (inactief)</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success pull-right" type="submit">Aanmaken</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    @foreach($teams as $team)
        @include('teams.edit')
    @endforeach
@endsection