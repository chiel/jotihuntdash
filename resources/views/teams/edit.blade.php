<!-- Modal -->
<div id="editTeam-{{ $team->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Team bewerken</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>

            </div>
            <div class="modal-body">
                <form method="POST" action="{{ action('TeamController@editTeam', $team->id) }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Team naam</label>
                        <input
                                name="name"
                                class="form-control"
                                id="name"
                                placeholder="Voorbeeld team..."
                                type="text"
                                value="{{ $team->name }}">
                    </div>

                    <div class="form-group">
                        <label for="members">Groeps leden</label>
                        <input
                                name="members"
                                class="form-control"
                                id="members"
                                placeholder="Jan, Klaas, Bart"
                                type="text"
                                value="{{ $team->getTeamMembersCSV() }}">
                        <small>Gescheiden door een ", "</small>
                    </div>

                    <div class="form-group">
                        <label for="phone">Telefoonnummer</label>
                        <input
                                name="phone"
                                class="form-control"
                                id="phone"
                                type="text"
                                value="{{ $team->phone}}">
                        <small>Zonder spaties of andere tekens, alleen nummmer aan elkaar geschreven</small>
                    </div>

                    <div class="form-group">
                        <label for="phone">Traccar device id</label>
                        <input
                                name="device_uniqueId"
                                class="form-control"
                                id="device_uniqueId"
                                value="{{ $team->device_uniqueId}}"
                                type="text">
                        <small>Zonder spaties of andere tekens, alleen nummmer aan elkaar geschreven</small>
                    </div>

                    <div class="form-group">
                        <label for="hunts_on">Hunt op</label>
                        <select name="hunts_on" class="form-control" id="hunts_on">
                            @foreach($teamNames as $teamName)
                                <option value="{{ $teamName }}"
                                        @if($teamName == $team->hunts_on_team)
                                        selected
                                        @endif> {{$teamName}}</option>
                            @endforeach
                            <option value="Niemand (inactief)"> Niemand (inactief)</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success pull-right" type="submit">Bewerken</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>