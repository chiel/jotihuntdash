<!-- Vossen Status -->
<div class="row">
    @foreach($vossen as $vos)
        <div class="col-lg-2 col-sm-6">
            <a href="{{ action('VosController@view', $vos['vos']->team) }}">
                <div class="widget stats-widget">
                    <div class="widget-body" style="padding: 8px;">
                        <div class="float-left">

                            <h3 class="widget-title text-primary" style="margin-bottom: 2px;">
                                {{ $vos['vos']->team }}
                                @if($vos['hunt'])
                                    <small style="color: red; font-size: 11px;">
                                        nieuwe hunt {{ date('H:i', strtotime('+1 hour', strtotime($vos['hunt']['time']))) }}
                                    </small>
                                @endif
                            </h3>

                            @if($vos['hint'])
                                <small class="text-color">{{ $vos['hint']['location_human'] }} <i>{{ date('H:i', strtotime($vos['hint']['hint_time'])) }}</i></small><br>
                            @endif
                            @if($vos['hunt'])
                                <small class="text-color">Hunt: {{ $vos['hunt']['location'] }} {{ date('H:i', strtotime($vos['hunt']['time'])) }}</small><br>
                            @endif
                            <hr style="margin: 5px 0;">
                            <small class="text-color">Hunt teams:</small><br>
                            <small class="text-color">
                                @foreach($hunters as $hunter)
                                    @if($hunter->hunts_on_team == $vos['vos']->team)
                                        <b>{{ $hunter->name }}</b> ({!! $hunter->printTeamMembers() !!})<br>
                                    @endif
                                @endforeach
                            </small>
                        </div>
                    </div>
                    @if($vos['vos']->status == 'rood')
                        <footer class="widget-footer bg-danger"></footer>
                    @elseif($vos['vos']->status == 'oranje')
                        <footer class="widget-footer bg-warning"></footer>
                    @elseif($vos['vos']->status == 'groen')
                        <footer class="widget-footer bg-success"></footer>
                    @endif
                </div>
            </a>
        </div>
    @endforeach
</div>