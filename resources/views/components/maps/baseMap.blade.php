<script src="/js/OpenLayers.js"></script>
{!! $defaultLong =  5.9642643;
    $defaultLat = 51.999572
    !!}
<script>
    map = new OpenLayers.Map("demoMap");
    map.addLayer(new OpenLayers.Layer.OSM());
    var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
    var toProjection = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
    var position = new OpenLayers.LonLat(/*expesssion expected is faulty */{{ $defaultLong }}, {{ $defaultLat }}).transform(fromProjection, toProjection);
    var zoom = 10;

    var kmlLayer = new OpenLayers.Layer.Vector("KML", {
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: new OpenLayers.Protocol.HTTP({
            url: "/jotihunt2018.kml",
            format: new OpenLayers.Format.KML({
                extractStyles: true,
                extractAttributes: true,
                maxDepth: 1
            })
        })
    });
    map.addLayer(kmlLayer);

    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);

    map.zoomToMaxExtent();
    map.setCenter(position, zoom);
</script>
