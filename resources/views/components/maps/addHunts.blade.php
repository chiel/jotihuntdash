<script>
    @if(isset($hunts))
        @foreach($hunts as $hunt)
            var loc{{ $hunt->id }}       = new OpenLayers.LonLat({{ $hunt->getLongLat() }}).transform( fromProjection, toProjection);
            markers.addMarker(new OpenLayers.Marker(loc{{ $hunt->id }} ));
        @endforeach
    @endif
</script>
