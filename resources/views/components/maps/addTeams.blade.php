<script>
    @if(isset($teams))
        @foreach($teams as $team)
            @if(isset($team->long) && isset($team->lat))
                var teamloc{{ $team->id }}       = new OpenLayers.LonLat({{ $team->getLongLat() }}).transform( fromProjection, toProjection);
                var teamIcon{{ $team->id }} = new OpenLayers.Icon('/js/img/marker-green.png');
                var teamMark{{ $team->id }} = new OpenLayers.Marker(teamloc{{ $team->id }}, teamIcon{{ $team->id }});
                markers.addMarker(teamMark{{ $team->id }});
            @endif
        @endforeach
    @endif
</script>
