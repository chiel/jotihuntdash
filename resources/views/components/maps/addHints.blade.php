<script>
    @if(isset($hints))
        @foreach($hints as $hint)
            var hintloc{{ $hint->id }}       = new OpenLayers.LonLat({{ $hint->getLongLat() }}).transform(fromProjection, toProjection);
            var hintIcon{{ $hint->id }} = new OpenLayers.Icon('/js/img/marker-blue.png');
            var hintmark{{ $hint->id }} = new OpenLayers.Marker(hintloc{{ $hint->id }}, hintIcon{{ $hint->id }});
            //hintmark{{ $hint->id }}.icon = '/js/img/marker-blue.png';

            markers.addMarker(hintmark{{ $hint->id }});
        @endforeach
    @endif
</script>
