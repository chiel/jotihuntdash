<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Jotihunt DASH</title>

    <!-- CSS -->
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/perfect-scrollbar.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/switchery.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">
    <link href="{{ asset('css/hamburgers.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/material-design-iconic-font.css') }}" rel="stylesheet">
    <link href="{{ asset('https://fonts.googleapis.com/css?family=Raleway:300,400,500,600') }}" rel="stylesheet">

    <script src="{{ asset('js/breakpoints.min.js') }}"></script>
    <script>Breakpoints({
            xs: {min: 0, max: 575},
            sm: {min: 576, max: 767},
            md: {min: 768, max: 991},
            lg: {min: 992, max: 1199},
            xl: {min: 1200, max: Infinity}
        });</script>

    <style>
        .map:-moz-full-screen {
            height: 100%;
        }
        .map:-webkit-full-screen {
            height: 100%;
        }
        .map:-ms-fullscreen {
            height: 100%;
        }
        .map:fullscreen {
            height: 100%;
        }
        .ol-rotate {
            top: 3em;
        }
    </style>

    @yield('head')
</head>
<body class="menubar-top menubar-light dashboard">
<!-- Begin Header -->
<nav class="site-navbar navbar fixed-top navbar-toggleable-md navbar-inverse bg-indigo-500">
    <div class="navbar-header">
        <a href="/" class="navbar-brand">
            <span class="brand-icon">
                <img src="{{ asset('img/fox.png') }}" height="45px;"/>
            </span>
            <span class="brand-name hidden-fold">Jotihunt</span>
        </a>

        <button data-toggle="menubar" class="mr-auto hidden-lg-up hamburger hamburger--collapse js-hamburger" type="button">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
        </button>
    </div>
</nav>
<!-- End Header -->


<!-- Begin Menubar -->
<aside class="site-menubar">
    <div class="menubar-scroll-wrapper">
        <ul class="site-menu">
            <li>
                <a href="/"><i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i> <span class="menu-text">Dashboard</span></a>
            </li>
            <li>
                <a href="/dash2"><i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i> <span class="menu-text">Hunters+kaart</span></a>
            </li>
            <li>
                <a href="{{ action('TeamController@index') }}"><i class="menu-icon zmdi zmdi-accounts zmdi-hc-lg"></i>
                    <span class="menu-text">Hunters</span></a>
            </li>
            <li>
                <a href="{{ action('DashboardController@map') }}"><i class="menu-icon zmdi zmdi-map zmdi-hc-lg"></i>
                    <span class="menu-text">Kaart</span></a>
            </li>

            <li class="pull-right">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="menu-icon zmdi zmdi-power zmdi-hc-lg"></i>
                    <span class="menu-text">Log uit</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>

            <li>
                <a href="tel:0624735183"><i class="menu-icon zmdi zmdi-phone zmdi-hc-lg"></i>
                    <span class="menu-text">Blokhut</span></a>
            </li>
        </ul>
    </div>
</aside>
<!-- End Menubar -->


<main class="site-main">
    <div class="site-content" style="padding: 3px;">
        @yield('page')
    </div>


    <!-- Begin footer -->
    <footer class="site-footer">
        <div class="copyright">Ontwikkeld door <a class="text-info" href="http://cbyte.nl/">Cbyte</a> & <a class="text-info" href="https://404solutions.nl/">404solutions</a>
            ©<?php echo date('Y'); ?></div>
    </footer>
    <!-- /.site-footer -->
</main>


<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/tether.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('js//main.js') }}"></script>
<script src="{{ asset('js/site.js') }}"></script>
<script src="{{ asset('js/menubar.js') }}"></script>

@if(isset($autorefresh) && $autorefresh)
    <script>
        $(document).ready(function() {
            var lastTimestamp = {{ \App\Http\Controllers\JotihuntApiController::getLastTimestamp() }};
            console.log(lastTimestamp);

            //Infinite loop to check for updates
            setInterval(function(){
                $.ajax({
                    type: "GET",
                    url: '{{ action('JotihuntApiController@shouldRefresh') }}',
                    success: function( msg ) {
                        if(msg['lastTimestamp'] > lastTimestamp) {
                            if(!$('.modal').hasClass('') && true) {
                                location.reload();
                            }
                        }
                    }
                });
            }, 8000);
        });
    </script>
@endif

@yield('footer')

</body>
</html>