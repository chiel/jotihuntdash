@extends('base')

@section('page')
    @include('components.vossen')

    <!-- /.row -->
    <div class="row">
        <!-- Nieuws -->
        <div class="col-lg-4 col-sm-6">
            <div class="widget stats-widget">
                <div class="widget-body">
                    <div class="float-left" style="width: 100%;">
                        <h3 class="widget-title text-primary">Nieuws</h3>
                        <div id="accordion" role="tablist">
                            @foreach($nieuws as $newsItem)
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading{{ $newsItem->id }}">
                                        <h5>
                                            <a data-toggle="collapse" href="#collapse{{ $newsItem->id }}" aria-expanded="true" aria-controls="collapse{{ $newsItem->id }}">
                                                {{ $newsItem->post_title }}
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse{{ $newsItem->id }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $newsItem->id }}"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            {!!  $newsItem->post_content !!}
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
                <footer class="widget-footer bg-primary"></footer>
            </div>
        </div>
        <!-- / Nieuws -->

        <!-- Hints -->
        <div class="col-lg-4 col-sm-6">
            <div class="widget stats-widget">
                <div class="widget-body">
                    <div class="float-left" style="width: 100%;">
                        <h3 class="widget-title text-primary">Hints</h3>
                        <div id="accordion" role="tablist">
                            @foreach($hints as $hint)
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading{{ $hint->id }}">
                                        <h5>
                                            <a data-toggle="collapse" href="#collapse{{ $hint->id }}" aria-expanded="true" aria-controls="collapse{{ $hint->id }}">
                                                {{ $hint->post_title }}
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse{{ $hint->id }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $hint->id }}"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            {!!  $hint->post_content !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <footer class="widget-footer bg-primary"></footer>
            </div>
        </div>
        <!-- / Hints -->

        <!-- Opdrachten -->
        <div class="col-lg-4 col-sm-6">
            <div class="widget stats-widget">
                <div class="widget-body">
                    <div class="float-left" style="width: 100%;">
                        <h3 class="widget-title text-primary">Opdrachten</h3>
                        <div id="accordion" role="tablist">
                            @foreach($opdrachten as $opdracht)
                                <div class="card">
                                    <div class="card-header" role="tab" id="heading{{ $opdracht->id }}">
                                        <h5>
                                            <a data-toggle="collapse" href="#collapse{{ $opdracht->id }}" aria-expanded="true" aria-controls="collapse{{ $opdracht->id }}">
                                                {{ $opdracht->post_title }}
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapse{{ $opdracht->id }}" class="collapse" role="tabpanel" aria-labelledby="heading{{ $opdracht->id }}"
                                         data-parent="#accordion">
                                        <div class="card-body">
                                            {!!  $opdracht->post_content !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <footer class="widget-footer bg-primary"></footer>
            </div>
        </div>
        <!-- / Opdrachten -->
    </div>
@endsection