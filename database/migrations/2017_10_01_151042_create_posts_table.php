<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_type');
            $table->integer('post_id')->unique();
            $table->text('post_title');
            $table->timestamp('post_update')->nullable();
            $table->longText('post_content');
            $table->timestamp('post_date')->nullable();
            $table->timestamp('assignment_endtime')->nullable();
            $table->integer('assignment_points')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
