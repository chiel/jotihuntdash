<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHuntteamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('huntteams', function ($table) {
            $table->dropColumn('location');
            $table->integer('device_uniqueId')->unique()->after('hunts_on_team')->nullable();
            $table->double('long', 9, 7)->after('device_uniqueId')->nullable();
            $table->double('lat', 9, 7)->after('long')->nullable();
            $table->integer('batterylevel')->after('lat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('huntteams', function ($table) {
            $table->string('location', 255)->after('team_members')->nullable();
            $table->dropColumn('device_uniqueId');
            $table->dropColumn('long');
            $table->dropColumn('lat');
            $table->dropColumn('batterylevel');
        });
    }
}
