#!/bin/bash

repoPath="/var/www/vhosts/mudman.nl/jotihunt.mudman.nl/"
npmPath="/opt/plesk/node/10/bin/npm"
phpPath="/opt/plesk/php/7.3/bin/php"
composerPath="/usr/lib64/plesk-9.0/composer.phar"
#npmPath="/usr/local/bin/npm"

# Notify
echo "---"
echo "Deploy script has been started."
echo "Any unsaved data in the app directory will be overwritten."
echo "If you DO NOT want this, press CTRL + C within 5 seconds."
echo "---"
echo "5"
sleep 1
echo "4"
sleep 1
echo "3"
sleep 1
echo "2"
sleep 1
echo "1"
sleep 1
echo "---"
echo "Deploy script started, please keep this window open during the process."
echo "---"
echo ""
# End notify
# Go to repo directory
cd $repoPath
echo "Changed directory..."
echo ""
$phpPath artisan down
echo "Set laravel in maintenance mode"
echo ""

# Install composer dep.
$phpPath $composerPath install --no-dev
echo "Composer has been updated"
echo ""
# discover packages
$phpPath artisan package:discover
echo "Packages discovered"
echo ""
# migrate
echo "Starting to migrate master database"
$phpPath artisan migrate --force
echo "Migration of master-database was successful"
echo ""
sleep 1

# Clear application and view cache
$phpPath artisan config:clear
$phpPath artisan cache:clear
$phpPath artisan view:clear
echo "Cache cleared..."
echo ""
# NPM production --> Asset compiling
$npmPath ci
$npmPath run production
echo "Assets successfully built in production mode."
echo ""
sleep 1
echo "Set laravel in production mode"
    echo ""
$phpPath artisan up
echo ""
# Closing
echo "---"
echo "Deploy script successfully completed."
echo "---"
